@extends('layouts.app')
@section('content')

    <h1>Nuevo evento</h1>
    <div class="form">
    <form  action="/events" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="summary" value="{{ old('summary') }}">
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Descripción: </label>
        <input type="text" name="description" value="{{ old('description') }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Desde: </label>
        <input type="date" name="start" value="{{ old('name') }}">
        <input type="time" name="startTime" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Hasta: </label>
        <input type="hidden" name="end" value="{{ old('name') }}">
        <input type="time" name="endTime" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>


    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')