@extends('layouts.app')
@section('content')

<p>
<a href="/orders/{{ $order->id }}/pdf">PDF</a> - 
<a href="/orders/{{ $order->id }}/email">Envio por Email</a>
</p>

    <h1>Pedido {{ $order->id }}</h1>
    <p>Fecha: {{ $order->created_at->format('d-m-Y') }} </p>
    <p>Cliente: {{ $order->user->name }} {{ $order->user->surname }} </p>
    <p>Email: {{ $order->user->email }} </p>




    <h2>Detalle</h2>
    <table class = "table">
    <tr>
        <th>Producto</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Importe</th>
    </tr>
    <?php
        $cost =  0;
    ?>
    @foreach ($order->products as $product)
        <?php
        $costElement =  $product->pivot->price * $product->pivot->quantity;
        $cost +=  $costElement;
        ?>
        <tr>
            <td>{{ $product->name }} </td>
            <td>{{ $product->pivot->price }}</td>
            <td>{{ $product->pivot->quantity }}</td>
            <td>{{ $costElement }}</td>
        </tr>
    @endforeach
    </table>
    Total: {{ $cost }} <br>
    Total: {{ $order->cost() }}
@endsection('content')