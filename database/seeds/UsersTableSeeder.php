<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => 1,
            'name' => 'Pepe',
            'surname' => 'García López',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
            ]);
        DB::table('users')->insert([
            'role_id' => 2,
            'name' => 'Juan',
            'surname' => 'Sánchez Moreno',
            'email' => 'juan@gmail.com',
            'password' => bcrypt('juan'),
            ]);
        DB::table('users')->insert([
            'role_id' => 2,
            'name' => 'Ana',
            'surname' => 'Merino Higuera',
            'email' => 'ana@gmail.com',
            'password' => bcrypt('ana'),
            ]);
        DB::table('users')->insert([
            'role_id' => 2,
            'name' => 'Yolanda',
            'surname' => 'Rodrigo Sorribas',
            'email' => 'yolanda@gmail.com',
            'password' => bcrypt('ana'),
            ]);
    }
}
