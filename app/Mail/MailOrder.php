<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;

class MailOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Order $order, \Barryvdh\DomPDF\PDF $pdf)
    {
        $this->order = $order;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@shop17.com')
                    ->subject('Confirmación de pedido')
                    ->view('emails.order2')
                     ->attachData($this->pdf->stream('order.pdf'), 'order.pdf', [
                        'mime' => 'application/pdf'
                        ]);
    }
}
